<?php

namespace Pta\Ab\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Ab extends Model
{
    protected $table = 'ab';

    protected $fillable = ['name', 'content'];

    public function entity()
    {
        return $this->morphTo();
    }

    public function getContentAttribute($content)
    {
        return $content ? new Collection(json_decode($content, true)) : [];
    }
    
    public function setContentAttribute(array $content)
    {
        $this->attributes['content'] = $content ? json_encode($content) : '';
    }
    
    public static function getModel(array $data = [])
    {
        $class = '\\' . ltrim('Pta\Ab\Models\Ab', '\\');
        
        return new $class($data);
    }
}
