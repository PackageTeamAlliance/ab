<?php 

namespace Pta\Ab\Traits;

use Pta\Ab\Models\Ab;

trait AbTrait
{

    /**
     * Returns the seo entry that belongs to this entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function getAb()
    {
        return $this->morphOne(Ab::class, 'entity');
    }

    public function getAbKey($key)
    {
        return collect($this->getAb->content)->get($key);
    }

     /**
     * Determines if the entity has a ab entry attached.
     *
     * @return bool
     */
    public function hasAb()
    {
        return (bool) $this->ab;
    }

     /**
     * Creates a new seo entry
     *
     * @param  array  $attributes
     * @return Ab
     */
    public function createAb(array $data)
    {
        return $this->getAb()->create($data);
    }

     /**
     * Updates the ab entry with the given attributes.
     *
     * @param  array  $attributes
     */
    public function updateAb(array $data)
    {
        $ab = $this->getAb;

        $ab->fill($data)->save();

        return $ab;
    }

    /**
     * Creates or Updates the ab entry with the given attributes.
     *
     * @param  array  $attributes
     * @return Ab
     */
    public function storeAb(array $attributes)
    {
        $method = ! $this->getAb ? 'createAb' : 'updateAb';

        return $this->{$method}($attributes);
    }

    /**
     * Deletes the ab entry that's attached to the entity.
     *
     * @return Ab
     */
    public function deleteAb()
    {
        return $this->getAb->delete();
    }
}
