<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ab', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('entity');
            $table->string('name')->length(255);
            $table->text('content');
            $table->integer('requests')->unsigned()->default(0);
            $table->integer('a_view')->unsigned()->default(0);
            $table->integer('a_clicks')->unsigned()->default(0);
            $table->integer('b_view')->unsigned()->default(0);
            $table->integer('b_clicks')->unsigned()->default(0);
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ab');
    }
}
